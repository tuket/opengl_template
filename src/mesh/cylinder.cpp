#include "cylinder.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <vector>

const float PI = glm::pi<float>();

using namespace std;

Mesh createCylinder(float r, float h, unsigned s)
{
	Mesh mesh;

	const unsigned nv = 4 * (s + 1);	// don't share the same normal for bases and side
	mesh.numVertices = nv;

	unsigned numTrianglesBase = s;
	unsigned numTrianglesSide = s * 2;
	const unsigned nt = 2 * numTrianglesBase + numTrianglesSide;
	mesh.numTriangles = nt;

	// POSITIONS
	vector<glm::vec3> positions;
	positions.reserve(nv);

	// side bot
	for (unsigned i = 0; i < s; i++)
	{
		float a = ((float)i / s) * 2 * PI;
		glm::vec3 p;
		p.y = -h / 2;
		p.x = cos(a) * r;
		p.z = sin(a) * r;
		positions.push_back(p);
	}
	positions.push_back(glm::vec3(r, -h/2, 0));	// replicate first vert

	// side top
	for (unsigned i = 0; i < s; i++)
	{
		float a = ((float)i / s) * 2 * PI;
		glm::vec3 p;
		p.y = h / 2;
		p.x = cos(a) * r;
		p.z = sin(a) * r;
		positions.push_back(p);
	}
	positions.push_back(glm::vec3(r, h/2, 0));	// replicate first vert

	// bot base
	for (unsigned i = 0; i < s; i++)
	{
		float a = ((float)i / s) * 2 * PI;
		glm::vec3 p;
		p.y = -h/2;
		p.x = cos(a) * r;
		p.z = sin(a) * r;
		positions.push_back(p);
	}

	// top base
	for (unsigned i = 0; i < s; i++)
	{
		float a = ((float)i / s) * 2 * PI;
		glm::vec3 p;
		p.y = h/2;
		p.x = cos(a) * r;
		p.z = sin(a) * r;
		positions.push_back(p);
	}

	// bot circle center
	positions.push_back(glm::vec3(0, -h/2, 0));
	// top circle center
	positions.push_back(glm::vec3(0, h/2, 0));

	// NORMALS
	vector<glm::vec3> normals;
	normals.reserve(nv);
	// side
	for (unsigned i = 0; i < 2 * s; i++)
	{
		glm::vec3 normal = glm::vec3(positions[i].x, 0, positions[i].z);
		normal /= normal.length();
		normals.push_back(normal);
	}

	// bot
	for (unsigned i = 0; i <= s; i++)
	{
		glm::vec3 normal(0, -1, 0);
		normals.push_back(normal);
	}

	// top
	for (unsigned i = 0; i <= s; i++)
	{
		glm::vec3 normal(0, +1, 0);
		normals.push_back(normal);
	}

	// bot center
	normals.push_back(glm::vec3(0, -1, 0));
	// top center
	normals.push_back(glm::vec3(0, +1, 0));

	// TANGENTS
	vector<glm::vec3> tangents;
	tangents.reserve(nv);
	// side
	for (unsigned i = 0; i < 2 * s + 2; i++)
	{
		glm::vec3 tangent(0, 1, 0);
		tangents.push_back(tangent);
	}

	// bot
	for (unsigned i = 2 * s; i < 3 * s; i++)
	{
		glm::vec3 tangent(0, 0, -1);
		tangents.push_back(tangent);
	}

	// top
	for (unsigned i = 2 * s; i < 3 * s; i++)
	{
		glm::vec3 tangent(0, 0, +1);
		tangents.push_back(tangent);
	}

	// bot center
	tangents.push_back( glm::vec3(0, 0, -1) );
	// top center
	tangents.push_back( glm::vec3(0, 0, +1) );

	// COLORS
	vector<glm::vec3> colors;
	colors.reserve(nv);
	// side
	for (unsigned i = 0; i <= s; i++)
	{
		glm::vec3 color(1, 0, 0);
		color *= abs(0.5 - ((float)i / s / 2));
		colors.push_back(color);
	}
	for (unsigned i = 0; i <= s; i++)
	{
		glm::vec3 color(1, 0, 0);
		color *= abs(0.5 - ((float)i / s / 2));
		colors.push_back(color);
	}

	// bot
	for (unsigned i = 2 * s; i < 3 * s; i++)
	{
		glm::vec3 color(0, 1, 0);
		colors.push_back(color);
	}

	// top
	for (unsigned i = 3 * s; i < 4 * s; i++)
	{
		glm::vec3 color(0, 0, 1);
		colors.push_back(color);
	}

	// bot center
	colors.push_back(glm::vec3(0, 1, 0));
	// top center
	colors.push_back(glm::vec3(0, 0, 1));

	// TEX COORDS
	vector<glm::vec2> texCoords;
	texCoords.reserve(nv);
	float radiusInTexture = r / (4 * r + h);
	float heightInTexture = h / (4 * r + h);
	// side
	for (unsigned i = 0; i <= s; i++)
	{
		float u = (float)(s-i) / s;
		float v = 2 * radiusInTexture;
		glm::vec2 texCoord(u, v);
		texCoords.push_back(texCoord);
	}
	for (unsigned i = 0; i <= s; i++)
	{
		float u = (float)(s-i) / s;
		float v = 2 * radiusInTexture + heightInTexture;
		glm::vec2 texCoord(u, v);
		texCoords.push_back(texCoord);
	}

	// bot
	float botCenterU = radiusInTexture;
	float botCenterV = radiusInTexture;
	for (unsigned i = 0; i < s; i++)
	{
		float u = botCenterU + radiusInTexture * cos((float)i / s * 2 * PI);
		float v = botCenterV + radiusInTexture * sin((float)i / s * 2 * PI);
		glm::vec2 texCoord(u, v);
		texCoords.push_back(texCoord);
	}

	// top
	float topCenterU = radiusInTexture;
	float topCenterV = 1 - radiusInTexture;
	for (unsigned i = 0; i < s; i++)
	{
		float u = topCenterU + radiusInTexture * cos((float)i / s * 2 * PI);
		float v = topCenterV + radiusInTexture * sin((float)i / s * 2 * PI);
		glm::vec2 texCoord(u, v);
		texCoords.push_back(texCoord);
	}

	// bot center
	{
		float u = radiusInTexture;
		float v = radiusInTexture;
		glm::vec2 texCoord(u, v);
		texCoords.push_back(texCoord);
	}
	// top center
	{
		float u = radiusInTexture;
		float v = 1 - radiusInTexture;
		glm::vec2 texCoord(u, v);
		texCoords.push_back(texCoord);
	}

	// TRIANGLES
	vector<glm::uvec3> triangles;
	triangles.reserve(nt);
	// side
	for (unsigned i = 0; i < s; i++)
	{
		glm::uvec3 tri1;
		{
			unsigned a = i;
			unsigned b = (s+1) + i;
			unsigned c = (s+1) + i + 1;
			tri1 = glm::uvec3(a, b, c);
		}
		glm::uvec3 tri2;
		{
			unsigned a = (s+1) + i + 1;
			unsigned b = i + 1;
			unsigned c = i;
			tri2 = glm::uvec3(a, b, c);
		}
		triangles.push_back(tri1);
		triangles.push_back(tri2);
	}

	unsigned botCenterId = nv - 2;
	// bot
	for (unsigned i = 0; i < s-1; i++)
	{
		unsigned a = botCenterId;
		unsigned b = 2 * (s+1) + i;
		unsigned c = 2 * (s+1) + i + 1;
		glm::uvec3 tri(a, b, c);
		triangles.push_back(tri);
	}
	{
		unsigned a = botCenterId;
		unsigned b = 2 * (s+1) + s - 1;
		unsigned c = 2 * (s+1);
		glm::uvec3 tri(a, b, c);
		triangles.push_back(tri);
	}

	unsigned topCenterId = nv - 1;
	// top
	for (unsigned i = 0; i < s - 1; i++)
	{
		unsigned a = topCenterId;
		unsigned b = 2 * (s+1) + s + i + 1;
		unsigned c = 2 * (s+1) + s + i;
		glm::uvec3 tri(a, b, c);
		triangles.push_back(tri);
	}
	{
		unsigned a = topCenterId;
		unsigned b = 2 * (s+1) + s;
		unsigned c = 2 * (s+1) + s + s - 1;
		glm::uvec3 tri(a, b, c);
		triangles.push_back(tri);
	}

	mesh.positions = new float[3 * nv];
	mesh.normals = new float[3 * nv];
	mesh.tangents = new float[3 * nv];
	mesh.colors = new float[3 * nv];
	mesh.texCoords = new float[2 * nv];
	mesh.triangles = new unsigned[3 * nt];

	memcpy(mesh.positions, &positions[0], 3 * nv * sizeof(float));
	memcpy(mesh.normals, &normals[0], 3 * nv * sizeof(float));
	memcpy(mesh.tangents, &tangents[0], 3 * nv * sizeof(float));
	memcpy(mesh.colors, &colors[0], 3 * nv * sizeof(float));
	memcpy(mesh.texCoords, &texCoords[0], 2 * nv * sizeof(float));
	memcpy(mesh.triangles, &triangles[0], 3 * nt * sizeof(unsigned));

	return mesh;
}