#include "mesh.hpp"

#include <glad/glad.h>
#include <stdio.h>
#include "../shader/shader.hpp"
#include <cstring>

// helper
inline void setAttribIfAvailable(int attribId, unsigned vbo, unsigned nv, unsigned nf, void* data)
{

	if (attribId != -1)
	{

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, nv * nf * sizeof(float),
			data, GL_STATIC_DRAW);
		glVertexAttribPointer(attribId, nf, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(attribId);
	}
	else
	{
		printf("warning: attrib not available in shader\n");
	}

}

void uploadMesh(MeshVAO& meshVAO, MeshVBOs& meshVBOs, const Mesh& mesh)
{
	glGenVertexArrays(1, &meshVAO);
	const unsigned numVBOs = sizeof(MeshVBOs) / sizeof(unsigned);
	glGenBuffers(numVBOs, (GLuint*)&meshVBOs);

	glBindVertexArray(meshVAO);

	const unsigned nv = mesh.numVertices;
	const unsigned nt = mesh.numTriangles;

	setAttribIfAvailable((int)PrePassShader::EAttribLocations::pos, meshVBOs.pos, nv, 3, mesh.positions);
	setAttribIfAvailable((int)PrePassShader::EAttribLocations::color, meshVBOs.color, nv, 3, mesh.colors);
	setAttribIfAvailable((int)PrePassShader::EAttribLocations::texCoord, meshVBOs.texCoord, nv, 2, mesh.texCoords);
	setAttribIfAvailable((int)PrePassShader::EAttribLocations::normal, meshVBOs.normal, nv, 3, mesh.normals);
	setAttribIfAvailable((int)PrePassShader::EAttribLocations::tangent, meshVBOs.tangent, nv, 3, mesh.tangents);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshVBOs.indices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, nt * 3 * sizeof(unsigned), mesh.triangles, GL_STATIC_DRAW);
}

void freeMeshVAO(MeshVAO& meshVAO)
{
	glDeleteBuffers(1, &meshVAO);
}

void freeMeshVBOs(MeshVBOs& meshVBOs)
{
	glDeleteBuffers(1, &meshVBOs.pos);
	glDeleteBuffers(1, &meshVBOs.color);
	glDeleteBuffers(1, &meshVBOs.texCoord);
	glDeleteBuffers(1, &meshVBOs.normal);
	glDeleteBuffers(1, &meshVBOs.tangent);
	glDeleteBuffers(1, &meshVBOs.indices);
}

void Mesh::initVertexData
(
	unsigned numVertices,
	const float* positions,
	const float* normals,
	const float* tangents,
	const float* colors,
	const float* texCoords
)
{
	unsigned nv = numVertices;
	this->numVertices = nv;
	
	this->positions = new float[3 * nv];
	this->normals = new float[3 * nv];
	this->tangents = new float[3 * nv];
	this->colors = new float[3 * nv];
	this->texCoords = new float[2 * nv];
	memcpy(this->positions, positions, 3 * nv * sizeof(float));
	memcpy(this->normals, normals, 3 * nv * sizeof(float));
	memcpy(this->tangents, tangents, 3 * nv * sizeof(float));
	memcpy(this->colors, colors, 3 * nv * sizeof(float));
	memcpy(this->texCoords, texCoords, 2 * nv * sizeof(float));
}

void Mesh::initTrianglesData
(
	unsigned numTriangles,
	unsigned* triangles
)
{
	unsigned nt = numTriangles;
	this->numTriangles = nt;

	this->triangles = new unsigned[3 * nt];
	memcpy(this->triangles, triangles, 3 * nt * sizeof(unsigned));
}