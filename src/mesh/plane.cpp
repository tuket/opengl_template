#include "plane.hpp"

#include <glad/glad.h>
#include "../shader/shader.hpp"

void createPlane(unsigned& VAO, unsigned& VBO)
{
	glGenBuffers(1, &VAO);
	glBindVertexArray(VAO);
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, planeNVertex * sizeof(float) * 3,
		planeVertexPos, GL_STATIC_DRAW);
	glVertexAttribPointer((int)PrePassShader::EAttribLocations::pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray((int)PrePassShader::EAttribLocations::pos);	// OPTIM: necessary?
}