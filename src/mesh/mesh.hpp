#ifndef MESH_HPP
#define MESH_HPP

struct Mesh
{
	
	unsigned numVertices;
	unsigned numTriangles;
	
	float* positions;
	float* normals;
	float* tangents;
	float* colors;
	float* texCoords;
	unsigned* triangles;

	void initVertexData
	(
		unsigned numVertices,
		const float* positions,
		const float* normals,
		const float* tangents,
		const float* colors,
		const float* texCoords
	);

	void initTrianglesData
	(
		unsigned numTriangles,
		unsigned* triangles
	);
	
};

struct VertexAttribLocations
{
	int pos;
	int color;
	int texCoord;
	int normal;
	int tangent;
};

struct MeshVBOs
{
	unsigned pos;
	unsigned color;
	unsigned texCoord;
	unsigned normal;
	unsigned tangent;
	// triangle indices
	unsigned indices;
};

typedef unsigned MeshVAO;

void uploadMesh(MeshVAO& meshVAO, MeshVBOs& meshVBOs, const Mesh& mesh);

static inline void freeMeshMemory(Mesh& mesh)
{
	delete[] mesh.positions;
	delete[] mesh.normals;
	delete[] mesh.tangents;
	delete[] mesh.colors;
	delete[] mesh.texCoords;
	delete[] mesh.triangles;
};

void freeMeshVAO(MeshVAO& meshVAO);
void freeMeshVBOs(MeshVBOs& meshVBOs);

#endif