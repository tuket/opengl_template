#include "tex_frag_shader.hpp"

#include "../util/auxiliar.hpp"
#include <glad/glad.h>
#include <cassert>
#include "../texture/render_target.hpp"

TexFragShader::TexFragShader(char* fragShadFile)
{
	compile(simpleQuadVSFileName, fragShadFile);

	glBindAttribLocation(program, 0, "inPos");

	link();
}

void TexFragShader::use()const
{
	Shader::use();
}

int TexFragShader::getUnifLoc(const char* name)
{
	return glGetUniformLocation(program, name);
}
