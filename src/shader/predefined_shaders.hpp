#ifndef PREDEFINED_SHADERS_HPP
#define PREDEFINED_SHADERS_HPP

namespace PreDefShad
{

	static const char* default = "default";
	static const char* bumpMapping = "bumpMapping";
	static const char* wireframe = "wireframe";
	static const char* simpleUnlitColor = "simpleUnlitColor";

}

#endif