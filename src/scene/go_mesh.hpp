#ifndef GO_MESH_HPP
#define GO_MESH_HPP

#include "game_object.hpp"

#include "../render/material.hpp"

typedef unsigned MeshId;

class GOMesh : public GameObject
{
public:

	GOMesh(SceneNode* sceneNode = nullptr);

	MeshId getMeshId()const { return meshId; }
	Material getMaterial()const { return material; }

	void setMeshFromFileName(const char* fileName);
	void setMeshFromCustomName(const char* name);

	void setMaterial(Material material) { this->material = material; }

	~GOMesh() {}


private:

	MeshId meshId;
	Material material;

};

#endif