#include "go_mesh.hpp"

#include "../mesh/mesh_pool.hpp"

GOMesh::GOMesh(SceneNode* sceneNode)
	: GameObject(sceneNode)
{
	material = Material::createBasicUnlit();
	material.setUnifValue("baseColor", glm::vec3(0.8, 0.8, 0.8));
}

void GOMesh::setMeshFromFileName(const char* fileName)
{
	MeshPool& pool = MeshPool::getInstance();
	meshId = pool.cacheMeshRequest(fileName);
}

void GOMesh::setMeshFromCustomName(const char* name)
{
	MeshPool& pool = MeshPool::getInstance();
	meshId = pool.getMeshId(name);
}