#ifndef SKYBOX_HPP
#define SKYBOX_HPP

unsigned loadCubamap(
	const char* positiveX, const char* negativeX,
	const char* positiveY, const char* negativeY,
	const char* positiveZ, const char* negativeZ
	);

unsigned instanceSkyboxVao();



#endif