#ifndef GAME_OBJECT_HPP
#define GAME_OBJECT_HPP

class SceneNode;

class GameObject
{

protected:

	GameObject(SceneNode* sceneNode = nullptr);

public:

	SceneNode* getSceneNode() { return sceneNode; }
	const SceneNode* getSceneNode()const { return sceneNode; }
	void setSceneNode(SceneNode* sceneNode) { this->sceneNode = sceneNode; }

	virtual ~GameObject() = 0 {};

private:

	SceneNode* sceneNode;

};

#endif