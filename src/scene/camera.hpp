#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera
{

public:

	Camera() : pos(0,0,0), heading(0), pitch(0) {}

	glm::vec3 getPosition()const;
	void setPosition(const glm::vec3& pos);

	float getHeading()const;
	void setHeading(float heading);

	float getPitch()const;
	void setPitch(float pitch);

	float getNear()const;
	void setNear(float nearPlane);

	float getFar()const;
	void setFar(float farPlane);

	float getFovY()const;
	void setFovY(float fovY);

	float getAspectRatio()const;
	void setAspectRatio(float aspectRatio);

	glm::vec3 getForwardVec()const;
	glm::vec3 getRightVec()const;

	glm::mat4 getViewMat()const;

	glm::mat4 getProjMat()const;

private:

	glm::vec3 pos;
	float heading, pitch;

	float nearPlane, farPlane;
	float fovY;
	float aspectRatio;

	// functions
	glm::mat3 getLocalRotMat()const;

};

#endif