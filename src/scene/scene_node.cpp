#include "scene_node.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include "scene.hpp"
#include <iostream>

void SceneNode::attachChild(SceneNode* node)
{
	node->scene = scene;
	children.insert(node);
	if (scene) scene->notifySceneNodeAttached(node);
}

void SceneNode::unattachFromParent()
{
	parent->children.erase(this);
	parent = nullptr;
	if (scene) scene->notifySceneNodeUnattached(this);
	scene = nullptr;
}

GameObject* SceneNode::getGameObject()
{
	return gameObject;
}

void SceneNode::setGameObject(GameObject* gameObject)
{
	this->gameObject = gameObject;
	gameObject->setSceneNode(this);
	if (scene)
	{
		scene->notifyGameObjectAdded(gameObject);
	}
}

void SceneNode::unsetGameObject()
{
	if (scene) scene->notifyGameObjectRemoved(this->gameObject);
	this->gameObject->setSceneNode(nullptr);
	this->gameObject = nullptr;
}

glm::mat4 SceneNode::getLocalModelMat()const
{
	glm::mat4 modelMat = glm::mat4_cast(rotation);
	modelMat = glm::translate(glm::mat4(), position) * modelMat;
	return modelMat;
}

glm::mat4 SceneNode::getModelMat()const
{
	glm::mat4 modelMat = getLocalModelMat();
	SceneNode*  p = parent;
	while (p)
	{
		modelMat = p->getLocalModelMat() * modelMat;
		p = p->parent;
	}
	return modelMat;
}

glm::vec3 SceneNode::getPosition()const
{
	return position;
}

glm::vec3 SceneNode::getWorldPosition()const
{
	glm::vec4 zeroPos(0, 0, 0, 1);
	return glm::vec3((getModelMat() * zeroPos));
}


glm::quat SceneNode::getRotation()const
{
	return rotation;
}

glm::quat SceneNode::getWorldRotation()const
{
	glm::quat rot = rotation;
	SceneNode* p = parent;
	while (p)
	{
		rot = p->getRotation() * rot;
		p = p->parent;
	}
	return rot;
}

glm::vec3 SceneNode::getForward()const
{
	glm::vec3 forward(0, 0, 1);
	forward = glm::rotate(rotation, forward);
	return forward;
}

glm::vec3 SceneNode::getRight()const
{
	glm::vec3 right(1, 0, 0);
	right = glm::rotate(rotation, right);
	return right;
}

glm::vec3 SceneNode::getUp()const
{
	glm::vec3 up(0, 1, 0);
	up = glm::rotate(rotation, up);
	return up;
}

void SceneNode::moveSelf(const glm::vec3& v)
{
	glm::vec3 selfV = glm::rotate(rotation, v);
	position += selfV;
}

void SceneNode::moveLocal(const glm::vec3& v)
{
	position += v;
}

void SceneNode::rotate(const glm::quat& rot)
{
	rotation = rotation * rot;
}

void SceneNode::rotateAroundParent(const glm::quat& rot)
{
	rotation = rotation * rot;
	position = glm::rotate(rot, position);
}