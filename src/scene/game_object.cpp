#include "game_object.hpp"

#include "scene_node.hpp"

GameObject::GameObject(SceneNode* sceneNode)
	: sceneNode(sceneNode)
{
	sceneNode->setGameObject(this);
}