#ifndef GO_CAMERA_HPP
#define GO_CAMERA_HPP

#include "game_object.hpp"
#include <glm/glm.hpp>

class GOCamera : public GameObject
{
public:

	GOCamera(SceneNode* sceneNode = nullptr,
		float fovY = 60.f, float aspectRatio = 1.f,
		float nearPlane = 0.2f, float farPlane = 500.f);

	float getNear()const;
	void setNear(float nearPlane);

	float getFar()const;
	void setFar(float farPlane);

	float getFovY()const;
	void setFovY(float fovY);

	float getAspectRatio()const;
	void setAspectRatio(float aspectRatio);

	glm::mat4 getViewMat()const;

	glm::mat4 getProjMat()const;

	virtual ~GOCamera() {}

private:

	float nearPlane, farPlane;
	float fovY;
	float aspectRatio;

};

#endif