#include "scene.hpp"

void Scene::setRoot(SceneNode* node)
{
	notifySceneNodeUnattached(root);
	notifySceneNodeAttached(node);
	root = node;
}

void Scene::notifyGameObjectAdded(GameObject* gameObject)
{
	if (gameObject == nullptr) return;

	if (auto o = dynamic_cast<GOPointLight*>(gameObject))
	{
		pointLights.insert(o);
	}
	else if (auto o = dynamic_cast<GODirLight*>(gameObject))
	{
		dirLights.insert(o);
	}
	else if (auto o = dynamic_cast<GOSpotLight*>(gameObject))
	{
		spotLights.insert(o);
	}
	else if (auto o = dynamic_cast<GOCamera*>(gameObject))
	{
		cameras.insert(o);
	}
	else if (auto o = dynamic_cast<GOMesh*>(gameObject))
	{
		meshes.insert(o);
	}
}

void Scene::notifyGameObjectRemoved(GameObject* gameObject)
{
	if (gameObject == nullptr) return;

	if (auto o = dynamic_cast<GOPointLight*>(gameObject))
	{
		pointLights.erase(o);
	}
	else if (auto o = dynamic_cast<GODirLight*>(gameObject))
	{
		dirLights.erase(o);
	}
	else if (auto o = dynamic_cast<GOSpotLight*>(gameObject))
	{
		spotLights.erase(o);
	}
	else if (auto o = dynamic_cast<GOCamera*>(gameObject))
	{
		cameras.erase(o);
	}
	else if (auto o = dynamic_cast<GOMesh*>(gameObject))
	{
		meshes.erase(o);
	}
}

void Scene::notifySceneNodeAttached(SceneNode* node)
{
	if (node == nullptr) return;

	if (auto o = node->getGameObject()) notifyGameObjectAdded(o);
	for (SceneNode* child : node->getChildern())
	{
		notifySceneNodeAttached(child);
	}
}

void Scene::notifySceneNodeUnattached(SceneNode* node)
{
	if (node == nullptr) return;

	if (auto o = node->getGameObject()) notifyGameObjectRemoved(o);
	for (SceneNode* child : node->getChildern())
	{
		notifySceneNodeUnattached(child);
	}
}