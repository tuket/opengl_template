#ifndef TEXTURE_HPP
#define TEXTURE_HPP

typedef int TextureId;

enum class TextureType
{
	RGB8,
	RGBA8,
	R8,
	R16,

	UNKNOWN
};

TextureId loadTextureFromFile(const char* fileName);

TextureId createEmptyTexture(
	unsigned w, unsigned h,
	TextureType texType = TextureType::RGBA8);

void createEmptyTextures(
	TextureId* output, unsigned numTextures,
	unsigned w, unsigned h,
	TextureType texType = TextureType::RGBA8
);

void createEmptyTextures(
	TextureId* output, unsigned numTextures,
	unsigned w, unsigned h,
	TextureType* texTypes);

void realeaseTexture(TextureId texId);

unsigned toGlInternalFormat(TextureType texType);

#endif