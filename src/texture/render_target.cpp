#include "render_target.hpp"

#include "../util/auxiliar.hpp"
#include <glad/glad.h>
#include <cassert>
#include <algorithm>

RenderTarget::RenderTarget(unsigned numTextures,
	unsigned w , unsigned h,
	TextureType texType)
{
	assert(numTextures <= MAX_NUM_TEXTURES);

	textures.resize(numTextures);
	createEmptyTextures(textures.data(), textures.size(), w, h);

	glGenFramebuffers(1, &fbo);

	textureTypes.resize(numTextures);
	fill(textureTypes.begin(), textureTypes.end(), texType);

	resize(w, h);
}

RenderTarget::RenderTarget(unsigned numTextures,
	unsigned w, unsigned h,
	TextureType* textureTypes)
{
	assert(numTextures <= MAX_NUM_TEXTURES);

	textures.resize(numTextures);
	createEmptyTextures(textures.data(), textures.size(), w, h, textureTypes);

	glGenFramebuffers(1, &fbo);

	this->textureTypes.resize(numTextures);
	this->textureTypes.assign(textureTypes, textureTypes + numTextures);

	resize(w, h);
}

TextureId RenderTarget::getTexture(unsigned slot)const
{
	return textures[slot];
}

void RenderTarget::resize(unsigned w, unsigned h)
{
	width = w;
	height = h;

	for (unsigned i = 0; i < getNumTextures(); i++)
	{
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, toGlInternalFormat(textureTypes[i]),
			w, h, 0,
			GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	for (unsigned i = 0; i < getNumTextures(); i++)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
			GL_TEXTURE_2D, textures[i], 0);
	}

	static auto initBuffs = []() {
		GLenum buffs[MAX_NUM_TEXTURES];
		for (unsigned i = 0; i < MAX_NUM_TEXTURES; i++)
			buffs[i] = GL_COLOR_ATTACHMENT0 + i;
		return buffs;
	};
	static const GLenum* buffs = initBuffs();

	glDrawBuffers(getNumTextures(), buffs);
		
	assert(GL_FRAMEBUFFER_COMPLETE == glCheckFramebufferStatus(GL_FRAMEBUFFER));

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

unsigned RenderTarget::getNumTextures()const
{
	return textures.size();
}

void RenderTarget::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}