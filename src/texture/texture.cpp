#include "texture.hpp"

#include "../util/auxiliar.hpp"
#include <iostream>
#include <glad/glad.h>
#include <stbi.h>
#include <map>

using namespace std;

unsigned TO_GL_INTERNAL_FORMAT[] =
{
	GL_RGB8,
	GL_RGBA8,
	GL_R8,
	GL_R16
};

map<unsigned, TextureType> FROM_GL_INTERNAL_FORMAT =
{
	{GL_RGB8, TextureType::RGB8},
	{GL_RGBA8, TextureType::RGBA8},
	{GL_R8, TextureType::R8},
	{GL_R16, TextureType::R16},
};

TextureId createEmptyTexture(
	unsigned w, unsigned h,
	TextureType texType)
{
	unsigned texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	
	glTexImage2D(GL_TEXTURE_2D,
		0,		// mip level
		TO_GL_INTERNAL_FORMAT[(unsigned)texType],	// internal format
		w, h,
		0,		// border :s
		GL_RGBA, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return (TextureId)texId;
}

void createEmptyTextures(
	TextureId* output, unsigned numTextures,
	unsigned w, unsigned h,
	TextureType texType
)
{
	glGenTextures(numTextures, (unsigned*)output);

	for (unsigned i = 0; i < numTextures; i++)
	{
		glBindTexture(GL_TEXTURE_2D, output[i]);

		glTexImage2D(GL_TEXTURE_2D,
			0,		// mip level
			TO_GL_INTERNAL_FORMAT[(unsigned)texType],	// internal format
			w, h,
			0,		// border :s
			GL_RGBA, GL_FLOAT, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	
}

void createEmptyTextures(
	TextureId* output, unsigned numTextures,
	unsigned w, unsigned h,
	TextureType* texTypes)
{
	glGenTextures(numTextures, (unsigned*)output);

	for (unsigned i = 0; i < numTextures; i++)
	{
		glBindTexture(GL_TEXTURE_2D, output[i]);

		glTexImage2D(GL_TEXTURE_2D,
			0,		// mip level
			TO_GL_INTERNAL_FORMAT[(unsigned)texTypes[i]],	// internal format
			w, h,
			0,		// border :s
			GL_RGBA, GL_FLOAT, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
}

TextureId loadTextureFromFile(const char* fileName)
{
	unsigned w, h;
	int width, height, bpp;
	unsigned char* data = stbi_load(fileName, &width, &height, &bpp, 4);
	w = width;
	h = height;

	if (!data)
	{
		std::cerr << "Error loading file: " << fileName << std::endl;
	}

	TextureId res;
	unsigned texId;

	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,			// mipmap
		GL_RGBA8,	// interno targeta grafica
		w, h,
		0,		// border
		GL_RGBA,	// formato cliente
		GL_UNSIGNED_BYTE,	// formato cliente
		(GLvoid*)data
	);

	stbi_image_free(data);

	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);	// trilinear
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	res = texId;
	return res;
}

void realeaseTexture(TextureId texId)
{
	unsigned uTexId = texId;
	glDeleteTextures(1, &uTexId);
}

unsigned toGlInternalFormat(TextureType texType)
{
	assert(texType < TextureType::UNKNOWN);
	return TO_GL_INTERNAL_FORMAT[(unsigned)texType];
}