#ifndef RENDER_TARGET_HPP
#define RENDER_TARGET_HPP

#include <vector>
#include "texture.hpp"

class RenderTarget
{

public:

	RenderTarget(unsigned numTextures,
		unsigned w = 0, unsigned h = 0,
		TextureType texType = TextureType::RGBA8);

	RenderTarget(unsigned numTextures,
		unsigned w, unsigned h,
		TextureType* textureTypes);

	TextureId getTexture(unsigned slot)const;

	void resize(unsigned w, unsigned h);

	unsigned getNumTextures()const;

	void bind();

	unsigned getWidth()const { return width; }
	unsigned getHeight()const { return height; }

	static const unsigned MAX_NUM_TEXTURES = 16;

private:

	std::vector<TextureType> textureTypes;
	std::vector<TextureId> textures;
	unsigned fbo;

	unsigned width, height;

};

#endif