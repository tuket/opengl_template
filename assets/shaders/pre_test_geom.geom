#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=128) out;

void main()
{
	for(int i=0; i<3; i++)
	{
		vec3 pos = gl_in[i].gl_Position.xyz * 0.5;
		gl_Position = vec4(pos, 1.0);
		EmitVertex();
	}
	EndPrimitive();
}