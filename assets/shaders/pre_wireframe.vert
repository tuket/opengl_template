#version 330 core

in vec3 inPos;
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;
in vec3 inTangent;

uniform mat4 modelViewProj;
uniform mat4 normal;

out vec3 varPos;
out vec3 varNormal;
out vec3 varTangent;
out vec2 varTexCoord;
out vec3 barycentric;

void main()
{
	int vid = gl_VertexID % 3;
	barycentric.x = (vid == 0 ? 1.0 : 0.0);
	barycentric.y = (vid == 1 ? 1.0 : 0.0);
	barycentric.z = (vid == 2 ? 1.0 : 0.0);

	varTexCoord = inTexCoord;
	varNormal = (normal * vec4(inNormal, 0.0)).xyz;
	varTangent = (normal * vec4(inTangent, 0.0)).xyz;

	varPos = vec3(modelViewProj * vec4 (inPos,1.0));
	gl_Position =  modelViewProj * vec4 (inPos,1.0);
}
