#version 330 core

out vec4 outColor;

in vec3 varTexCoord;

uniform samplerCube colorTex;

void main()
{    
    outColor = texture(colorTex, varTexCoord);
}