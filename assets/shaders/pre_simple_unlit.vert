#version 330 core

in vec3 inPos;
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;
in vec3 inTangent;

uniform mat4 modelViewProj;
uniform mat4 normal;

out vec3 varPos;
out vec3 varNormal;

void main()
{
	varNormal = (normal * vec4(inNormal, 0.0)).xyz;

	varPos = vec3(modelViewProj * vec4 (inPos,1.0));
	gl_Position =  modelViewProj * vec4 (inPos,1.0);
}
