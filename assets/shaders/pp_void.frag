#version 330 core

//Color de salida
out vec4 outColor;

//Variables Variantes
in vec2 varTexCoord;

//Textura
uniform sampler2D colorTex;

void main()
{
	vec4 color = textureLod(colorTex, varTexCoord, 0);
	outColor = color;
}
